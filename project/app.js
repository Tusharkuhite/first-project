const port = 8080;
const mongoose = require("mongoose");
const url ="mongodb+srv://admin:admin123@cluster0.orc4u.mongodb.net/?retryWrites=true&w=majority"
mongoose.connect(url, { useNewUrlParser: true , useUnifiedTopology: true})
	.then( () => console.log("Connected successfully...") )
	.catch( (err) => console.log(err) );

const projectSchema = new mongoose.Schema({
	id     : Number,
	name   : String,
	age    : Number,
	city   : String,
        salary : Number
});

const project = new mongoose.model("projects", projectSchema);

/** Express Mongoose Integration **/

const express = require("express");
const app = express();
const cors = require('cors');
app.use(cors());


const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

app.route("/")
.get(async (req, res) => {
	let result = await project.find();
	res.send(result);
                         }
    )
.post(async (req, res , next) => {
	//req_data = req.query;
	//console.log(req.body);
        
	try {
		let obj = new project(req.body);
		let result = await obj.save();
		console.log(result);
		res.send(result);
	}
	catch (err)  {
			res.status(400).json({message :err.message});
	}
})

.put(async (req, res) => {
	console.log(req.body);
	let result = await project.updateOne({"_id": req.body._id}, {
		"$set": {
			 "id"     : req.body.id,
                         "name"   : req.body.name,
                         "age"    : req.body.age,
                         "city"   : req.body.city,
                         "salary" : req.body.salary,
		}
	});

	res.send(result);
})
.delete( async (req, res) => {
	let result = await project.deleteOne({"_id": req.body._id});
	res.send(result);

})

 app.listen(process.env.PORT || port, () => {
	console.log("listening 8080...");
})



